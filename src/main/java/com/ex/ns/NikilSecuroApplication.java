package com.ex.ns;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NikilSecuroApplication {

	public static void main(String[] args) {
		SpringApplication.run(NikilSecuroApplication.class, args);
	}

}
