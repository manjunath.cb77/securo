package com.ex.ns.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ex.ns.dto.FileDto;
import com.ex.ns.dto.LoginDto;
import com.ex.ns.dto.UserDto;
import com.ex.ns.entity.FileEntity;
import com.ex.ns.entity.UserEntity;
import com.ex.ns.service.NsService;

@RestController
@RequestMapping(value = "/user")
@CrossOrigin(origins = "http://localhost:4200")
public class NsController {

	@Autowired
	private NsService service;
	
	@PostMapping(value = "/saveuser")
	public void saveuser(@RequestBody UserDto userDto) {
		System.out.println(userDto);
		service.saveuser(userDto);
	}
	
	@GetMapping(value = "/getuser")
	public@ResponseBody UserEntity getuser(LoginDto loginDto) {
		return service.getUser(loginDto);
	}
	
	@PostMapping(value = "savefile") 
	public void savefile(@RequestBody FileDto fileDto) {
		service.savefile(fileDto);
	}
	
	@GetMapping(value = "/getfiles")
	public@ResponseBody List<FileEntity> getfiles() {
		return service.getfiles();
	}
	

}
