package com.ex.ns.dto;

import java.util.Date;

import lombok.Data;

@Data
public class FileDto {
	
	private String name;
	private long size;
	private Date time;
	private String type;
	
	
	
//	 name: string;
//	  size: number;
//	  time: Date;
//	  type: string;
}
