package com.ex.ns.dto;

import lombok.Data;

@Data
public class UserDto {

	private String name;

	private String username;

	private String password;
}
