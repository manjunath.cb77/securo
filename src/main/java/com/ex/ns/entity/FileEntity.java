package com.ex.ns.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;
@Data
@Entity
@Table(name = "fileinfo")
public class FileEntity {
	
	@Id
	@GenericGenerator(name = "auto" , strategy = "increment")
	@GeneratedValue(generator = "auto")
	@Column(name = "id")
	private long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "size")
	private long size;
	
	@Column(name = "time")
	private Date time;
	
	@Column(name = "type")
	private String type;
}
