package com.ex.ns.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ex.ns.entity.FileEntity;

public interface FileRepository extends JpaRepository<FileEntity, Long>{

}
