package com.ex.ns.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ex.ns.entity.UserEntity;

public interface UserRepository extends JpaRepository<UserEntity, Long>{
	
	
	Optional<UserEntity> findByUsername(String username);

}
