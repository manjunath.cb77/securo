package com.ex.ns.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ex.ns.dto.FileDto;
import com.ex.ns.dto.LoginDto;
import com.ex.ns.dto.UserDto;
import com.ex.ns.entity.FileEntity;
import com.ex.ns.entity.UserEntity;
import com.ex.ns.repository.FileRepository;
import com.ex.ns.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class NsService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private FileRepository fileRepository;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public void saveuser(UserDto userDto) {
		UserEntity entity=objectMapper.convertValue(userDto, UserEntity.class);
		userRepository.save(entity);
	}

	public UserEntity getUser(LoginDto loginDto) {
//		String name=loginDto.getUsername();
//		String pass=loginDto.getPassword();
//		
//		UserEntity entity=jdbcTemplate.query("select * from userinfo where username='"+name
//				+"' and password='"+pass+"';", );
//		
		System.out.println(loginDto);
		UserEntity entity;
		Optional<UserEntity> optional=userRepository.findByUsername(loginDto.getUsername());
		System.out.println(optional);
		if(optional.isPresent()) {
			 entity=optional.get();
			 System.out.println(entity);
			 if((loginDto.getPassword()).equals(entity.getPassword())){
					return entity;
				}
		}else {
			
		}
		return null;
		
	
		
		
	}

	public void savefile(FileDto fileDto) {
		FileEntity entity=objectMapper.convertValue(fileDto, FileEntity.class);
		fileRepository.save(entity);
		
	}

	public List<FileEntity> getfiles() {
		List<FileEntity> list=fileRepository.findAll();
		return list;
	}
	
	

}
